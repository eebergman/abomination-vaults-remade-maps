## 2.2.1
    * Updated maps for level 5 (training grounds), level 6 (laboratory) and level 7 (prison). There have been some changes to the wall layout, so you may notice walls not lining up. 
    * Added lighting for levels 5, 6 and 7.

## 2.1.2
    * Updated level 3 and 4 maps with new versions. Images for the old version are in the legacy folder. 
    * There have been a significant number of changes to the layout of the cave and cavern walls. If you are using an existing scene, you will notice that your walls no longer line up. 

# 2.1.1
    * Beginning of graphical refresh for all maps - if you prefer the old ones, they are in the "legacy" folder
    * Updated level 1 and 2 maps with new versions
    * Adjusted level 1 walls - mostly around the arrow slits. These no longer allow vision from "outside" the keep.

## 1.3.3
    * Added OTARI map to Book 1.

## 1.3.2
    * Made the teleport circle in the training grounds more obvious.

## 1.3.1
    * Various tweaks to map images and walls.

# 1.3.0

* Book 3 maps Added

    #### Book 3
    * Level 8
    * Level 9
    * Level 10
    * Level 9 (50px grid image version - no scene yet).

    #### Book 2
    * Level 7 - alternative colour scheme (just swap the image).

    #### Book 1
    * Level 1 - alternative cleaner version with increased contract between floors/walls (just swap the image).

## 1.2.1

    * Added wall to level 4 Forever Stairs secret door. Added missing door to level 7 forge.

# 1.2.0

* Next book update!

    #### Book 2
    * Level 5
    * Level 6
    * Level 7
    * Smugglers' Refuge

## 1.1.3

    * Fixed manifest URL on Foundry repository.

## 1.1.2

    * Added missing door in level 4 D21 - SpartanCPA
    * Fixed terrain walls in lighthouse - Freeman

## 1.1.1

    * Book 1 wall layout fixes

# 1.1.0

* Initial release

    #### Book 1
    * Level 1
    * Level 2
    * Level 3
    * Level 4
    * Graveyard (draft version) 
